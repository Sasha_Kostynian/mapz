﻿using System;

namespace Prototype
{
    enum EnemyType
    {
        DefaultGhost, BoostedGhost
    }
    public interface Enemy
    {
        Enemy copyEnemy();
    }

    class DefaultGhost : Enemy
    {
        private EnemyType GhostType { get; }
        public Enemy copyEnemy()
        {
            Console.WriteLine("New enemy is being created at the moment");
            return (Enemy)this.MemberwiseClone();
        }

        public DefaultGhost()
        {
            GhostType = EnemyType.DefaultGhost;
        }
    }


    class Client
    {
        public void Main()
        {
            Prototype.Enemy GH = new DefaultGhost();
            Prototype.Enemy GH1 = GH.copyEnemy();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            new Client().Main();
        }
    }
}