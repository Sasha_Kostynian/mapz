﻿using System;

namespace Singleton
{
    class Map
    {
        private Map() { }

        private static Map locationsCount_instance;

        public static Map GetLocationsCount()
        {
            if (locationsCount_instance == null)
            {
                locationsCount_instance = new Map();
            }
            return locationsCount_instance;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Map s1 = Map.GetLocationsCount();
            Map s2 = Map.GetLocationsCount();

            if (s1 == s2)
            {
                Console.WriteLine("Everything OK, both locationCounts have same instance.");
            }
            else
            {
                Console.WriteLine("Singleton failed, locationCounts contain different instances.");
            }
        }
    }
}