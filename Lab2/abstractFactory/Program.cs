﻿using System;

namespace AbsFactory
{

    public interface WeaponFactory
    {
        MeeleRangeWeapon CreateMeeleWeapon();
        LongRangeWeapon CreateLongRangeWeapon();
    }

    class Weapons : WeaponFactory
    {
        public MeeleRangeWeapon CreateMeeleWeapon()
        {
            return new Sword();
        }

        public LongRangeWeapon CreateLongRangeWeapon()
        {
            return new Gun();
        }
    }


    public interface MeeleRangeWeapon
    {
        string couseDamade();
    }


    class Sword : MeeleRangeWeapon
    {
        public string couseDamade()
        {
            return "Damage coused by meele weapon.";
        }
    }

    public interface LongRangeWeapon
    {
        string causeDamage();
    }

    class Gun : LongRangeWeapon
    {
        public string causeDamage()
        {
            return "Damage coused by ranged weapon.";
        }
   
    }

    class Client
    {
        public void Main()
        {
            Console.WriteLine("Testing weapons");
            ClientMethod(new Weapons());
        }

        public void ClientMethod(WeaponFactory factory)
        {
            var MW = factory.CreateMeeleWeapon();
            var RW = factory.CreateLongRangeWeapon();

            Console.WriteLine(MW.couseDamade());
            Console.WriteLine(RW.causeDamage());
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            new Client().Main();
        }
    }
}