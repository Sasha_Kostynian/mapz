﻿using System;

namespace State
{

    class Context
    {
        private State _state = null;

        public Context(State state)
        {
            this.TransitionTo(state);
        }

        public void TransitionTo(State state)
        {
            Console.WriteLine($"Context: Transition to {state.GetType().Name}.");
            this._state = state;
            this._state.SetContext(this);
        }

        public void Request1()
        {
            this._state.DoResearch();
        }

        public void Request2()
        {
            this._state.RefreshHealth();
        }
    }
    abstract class State
    {
        protected Context _context;

        public void SetContext(Context context)
        {
            this._context = context;
        }

        public abstract void DoResearch();

        public abstract void RefreshHealth();
    }
    class ImprovedPlayer : State
    {
        public override void DoResearch()
        {
            Console.WriteLine("ImprovedPlayer handles request1.");
            Console.WriteLine("It will take 5 minutes to finish research");
        }

        public override void RefreshHealth()
        {
            Console.WriteLine("ImprovedPlayer handles request2.");
            Console.WriteLine("Your HP will be restored  in 14 minutes");
        }
    }

    class RegularPlayer : State
    {
        public override void DoResearch()
        {
            Console.WriteLine("RegularPlayer handles request1.");
            Console.WriteLine("It will take 8 minutes to finish research");
        }

        public override void RefreshHealth()
        {
            Console.WriteLine("RegularPlayer handles request2.");
            Console.WriteLine("Your HP will be restored  in 20 minutes");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var context = new Context(new ImprovedPlayer());
            context.Request1();
            context.Request2();
            var context2 = new Context(new RegularPlayer());
            context2.Request1();
            context2.Request2();
        }
    }
}
