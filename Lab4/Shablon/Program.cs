﻿using System;

namespace Shablon
{
    abstract class EnemyAI
    {
        public void TemplateMethod()
        {
            this.Move();
            this.Upgrade();
            this.Attack();
            this.BuildSmth();
            this.Reheal();
        }

        protected void Move()
        {
            Console.WriteLine("EnemyAI says: I am moving");
        }

        protected void Attack()
        {
            Console.WriteLine("EnemyAI says: Prepare for the attack");
        }

        protected abstract void Upgrade();

        protected abstract void Reheal();

        protected virtual void BuildSmth() { }

    }

    class EnemyPeople : EnemyAI
    {
        protected override void Upgrade()
        {
            Console.WriteLine("EnemyPeople says: I'm upgrading as a person");
        }

        protected override void Reheal()
        {
            Console.WriteLine("EnemyPeople says: I'm rehealing faster than monsters");
        }

        protected override void BuildSmth()
        {
            Console.WriteLine("EnemyPeople says: I'm bulding the house");
        }
    }

    class Monster : EnemyAI
    {
        protected override void Upgrade()
        {
            Console.WriteLine("Monster says: I'm upgrading as a monster");
        }

        protected override void Reheal()
        {
            Console.WriteLine("Monster says: I'm rehealing slower than the people");
        }
    }
    class Client
    {
        public static void ClientCode(EnemyAI abstractClass)
        {

            abstractClass.TemplateMethod();

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Same client code can work with different subclasses:");

            Client.ClientCode(new EnemyPeople());

            Console.Write("\n");

            Console.WriteLine("Same client code can work with different subclasses:");
            Client.ClientCode(new Monster());
        }
    }
}
