﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Memento
{
    class Level
    {
        private string _state;
        private int _points;

        public Level(string state, int points)
        {
            this._state = state;
            this._points = points;
            Console.WriteLine("Level: My initial state is: " + state + "\nPoints = " + _points.ToString());
        }

        public void DoSomething()
        {
            Console.WriteLine("Level: I'm changing a state.");
            this._state = this.GenerateRandomString(30);
            Console.WriteLine($"Level: and my state has changed to: {_state}  Points = {_points}");
        }

        private string GenerateRandomString(int length = 10)
        {
            string allowedSymbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string result = string.Empty;

            while (length > 0)
            {
                result += allowedSymbols[new Random().Next(0, allowedSymbols.Length)];

                Thread.Sleep(12);

                length--;
            }

            _points = new Random().Next(0, 1000);

            return result;
        }

        public IMemento Save()
        {
            return new ConcreteMemento(this._state, this._points);
        }

        public void Restore(IMemento memento)
        {
            if (!(memento is ConcreteMemento))
            {
                throw new Exception("Unknown memento class " + memento.ToString());
            }

            this._state = memento.GetState();
            this._points = memento.GetPoints();
            Console.Write($"Level: My state has changed to: {_state},   Points: {_points}");
        }
    }
    public interface IMemento
    {
        string GetName();

        int GetPoints();

        string GetState();

        DateTime GetDate();
    }

    class ConcreteMemento : IMemento
    {
        private string _state;
        private int _points;

        private DateTime _date;

        public ConcreteMemento(string state, int points)
        {
            this._state = state;
            this._points = points;
            this._date = DateTime.Now;
        }


        public string GetState()
        {
            return this._state;
        }


        public string GetName()
        {
            return $"{this._date} / ({this._state.Substring(0, 9)})...   Points = {this._points}";
        }

        public int GetPoints()
        {
            return _points;
        }

        public DateTime GetDate()
        {
            return this._date;
        }
    }

    class Caretaker
    {
        private List<IMemento> _mementos = new List<IMemento>();

        private Level _originator = null;

        public Caretaker(Level originator)
        {
            this._originator = originator;
        }

        public void Backup()
        {
            Console.WriteLine("\nCaretaker: Saving Originator's state...");
            this._mementos.Add(this._originator.Save());
        }

        public void Undo()
        {
            if (this._mementos.Count == 0)
            {
                return;
            }

            var memento = this._mementos.Last();
            this._mementos.Remove(memento);

            Console.WriteLine("Caretaker: Restoring state to: " + memento.GetState() +/*memento.GetName() +*/ "   Points = " + memento.GetPoints().ToString());

            try
            {
                this._originator.Restore(memento);
            }
            catch (Exception)
            {
                this.Undo();
            }
        }

        public void ShowHistory()
        {
            Console.WriteLine("Caretaker: Here's the list of mementos:");

            foreach (var memento in this._mementos)
            {
                Console.WriteLine(/*memento.GetName()*/"State: " + memento.GetState() +
                "   Points = " + memento.GetPoints().ToString());
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Level Lv = new Level("Start", 100);
            Caretaker caretaker = new Caretaker(Lv);
            ConcreteMemento CM = new ConcreteMemento("Start", 100);
            caretaker.Backup();
            Lv.DoSomething();
            caretaker.Backup();
            //Lv.DoSomething();
            Lv.Restore(CM);
            //caretaker.Backup();
            caretaker.Backup();
            caretaker.ShowHistory();
            caretaker.Undo();
        }
    }
}
