﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace Serial
{
    public interface IPublisher
    {
        void Attach(IObserver observer);

        void Detach(IObserver observer);

        void Notify();
    }
    public class Publisher : IPublisher
    {
        public int State { get; set; } = -0;
        private List<IObserver> _observers = new List<IObserver>();
        public void Attach(IObserver observer)
        {
            Console.WriteLine("Publisher: Attached an observer.");
            this._observers.Add(observer);
        }
        public void Detach(IObserver observer)
        {
            this._observers.Remove(observer);
            Console.WriteLine("Publisher: Detached an observer.");
        }
        public void Notify()
        {
            Console.WriteLine("Publisher: Notifying observers...");

            foreach (var observer in _observers)
            {
                observer.Update(this);
            }
        }
        public void SomeLogic()
        {
            Console.WriteLine("\nPublisher: I'm doing something important.");
            this.State = new Random().Next(0, 10);

            Thread.Sleep(15);

            Console.WriteLine("Publisher: My state has just changed to: " + this.State);
            this.Notify();
        }
    }
    public interface IObserver
    {
        void Update(IPublisher subject);
    }

    class ConcreteObserverA : IObserver
    {
        public void Update(IPublisher subject)
        {
            if ((subject as Publisher).State < 3)
            {
                Console.WriteLine("ObserverA: Reacted to the event.");
            }
        }
    }

    class ConcreteObserverB : IObserver
    {
        public void Update(IPublisher subject)
        {
            if ((subject as Publisher).State == 0 || (subject as Publisher).State >= 2)
            {
                Console.WriteLine("ObserverB: Reacted to the event.");
            }
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            var subject = new Publisher();
            var observerA = new ConcreteObserverA();
            subject.Attach(observerA);

            var observerB = new ConcreteObserverB();
            subject.Attach(observerB);

            subject.SomeLogic();
            subject.SomeLogic();

            subject.Detach(observerB);

            subject.SomeLogic();
        }
    }
}
