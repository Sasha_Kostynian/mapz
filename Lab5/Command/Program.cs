﻿using System;

namespace Command
{
    class Jasper
    {
        private IPerk _onStart;

        private IPerk _onFinish;

        public void SetOnStart(IPerk command)
        {
            this._onStart = command;
        }

        public void SetOnFinish(IPerk command)
        {
            this._onFinish = command;
        }


        public void DoSomethingImportant()
        {
            Console.WriteLine("Jasper: Increase HP on 25 points!");
            if (this._onStart is IPerk)
            {
                this._onStart.Execute();
            }

            Console.WriteLine("Jasper: Increase speed on 40 points!");
            if (this._onFinish is IPerk)
            {
                this._onFinish.Execute();
            }
        }
    }

    class Character
    {
        public void HPIncrease(int a)
        {
            Console.WriteLine($"Character: HP Increased on {a}");
        }

        public void SpeedIncrease(int b)
        {
            Console.WriteLine($"Character: Speed Increased on {b}");
        }
    }


    interface IPerk
    {
        void Execute();
    }

    class HPAddPerk : IPerk
    {
        private Character _receiver;

        private int _hp;


        public HPAddPerk(Character receiver, int hp)
        {
            this._receiver = receiver;
            this._hp = hp;
        }

        public void Execute()
        {
            Console.WriteLine($"HPAddPerk: Increasing HP on {_hp}");
            this._receiver.HPIncrease(this._hp);
        }
    }

    class SpeedAddPerk : IPerk
    {
        private Character _receiver;

        private int _speed;


        public SpeedAddPerk(Character receiver, int speed)
        {
            this._receiver = receiver;
            this._speed = speed;
        }

        public void Execute()
        {
            Console.WriteLine($"SpeedAddPerk: Increasing HP on {_speed}");
            this._receiver.SpeedIncrease(this._speed);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Jasper invoker = new Jasper();
            invoker.DoSomethingImportant();
            Character Casper = new Character();
            Casper.HPIncrease(80);
            SpeedAddPerk SAP = new SpeedAddPerk(Casper, 120);
            SAP.Execute();
        }
    }
}
