﻿using System;

namespace Facad1
{
    static class LevelLoader
    {
        static public string loadLevel()
        {
            LevelInfo levelInfo = new LevelInfo();
            string result = levelInfo.setInfo();
            result += LevelBody.setLevel();

            return result;

        }
    }

    static class LevelBody
    {

        public static string setLevel()
        {
            return "\nLevel discription: dungeon";
        }
    }

    class LevelInfo
    {
        public int number;
        public string name;
        public string description;

        public LevelInfo()
        {
            number = 1;
            name = "Hero";
            description = "True Hero";

        }

        public string setInfo()
        {
            return "This is level #" + number + "\n" + name + "\nDescription:\n" + description + "\n";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string LL = LevelLoader.loadLevel();
            Console.WriteLine(LL);

        }
    }
}
