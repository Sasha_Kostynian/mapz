﻿using System;

namespace Bridge
{
    class Player
    {
        public Weapon weapon;
        public string name;
        public int power;

        public string write()
        {
            return "This is player " + name + " with weapon: " + weapon.name + "\nHe shoots with sound: " + weapon.fire() + "\n\n";
        }
    }
    class Punisher : Player
    {
        public Punisher(Weapon w)
        {
            name = "Punisher";
            weapon = w;
            power = 80;
        }
    }
    class Destroyer : Player
    {
        public Destroyer(Weapon w)
        {
            name = "Destroyer";
            weapon = w;
            power = 100;
        }
    }
    class Weapon
    {
        public string name;
        public int weight;

        virtual public string fire()
        {
            return "Bang!";
        }
    }
    class Minigun : Weapon
    {
        public Minigun()
        {
            name = "Minigun";
            weight = 12;
        }

        override public string fire()
        {
            return "BBBBBBBBBBBBBBBang!!!";
        }
    }
    class Rifle : Weapon
    {
        public Rifle()
        {
            name = "Rifle";
            weight = 12;
        }

        override public string fire()
        {
            return "Bang Bang Bang Bang!!";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Player P = new Player();
            Minigun M = new Minigun();
            Destroyer D = new Destroyer(M);
            Rifle R = new Rifle();
            Punisher Pun = new Punisher(R);
            Console.WriteLine(D.write());
            Console.WriteLine(Pun.write());
        }
    }
}
