﻿using System;
using System.Collections.Generic;

namespace Flyweight
{
    class Forest
    {
        public List<Tree> trees = new List<Tree>();

        public Forest()
        {
            addTrees(trees);
        }

        public void addTrees(List<Tree> trees)
        {
            Random rand = new Random();
            TreeFactory factory = new TreeFactory();
            factory.addTypes();
            for (int i = 0; i < 6; i++)
            {
                int randh = rand.Next(10, 50);
                int randw = rand.Next(1, 5);
                int randt = rand.Next(0, 3);
                trees.Add(new Tree(randh, randw, factory.types[randt]));
            }
        }

        public string showTrees()
        {
            string result = "";
            foreach (Tree tree in trees)
            {
                result += tree.setTree();
            }
            return result;
        }
    }

    class Tree
    {
        public static int amount;
        int number;
        int height;
        int width;
        TreeType type;


        public Tree(int height, int width, TreeType type)
        {
            this.height = height;
            this.width = width;
            this.type = type;
            amount++;
            this.number = amount;
        }

        public string setTree()
        {
            return "This is tree #" + number + "\nName: " + type.name + "\nColor: " + type.color + "\nSize: " + height.ToString() + " x " + width.ToString() + "\n\n";
        }
    }

    class TreeFactory
    {
        public List<TreeType> types;

        public TreeFactory()
        {
            types = new List<TreeType>();
        }

        public void addTypes()
        {
            types.Add(new TreeType("Dub", "Dark Green"));
            types.Add(new TreeType("Bereza", "Brihgt Green"));
            types.Add(new TreeType("Sosna", "Green"));
        }
    }

    class TreeType
    {
        public string name;
        public string color;
        public TreeType(string name, string color)
        {
            this.name = name;
            this.color = color;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Forest F = new Forest();
            string S = F.showTrees();
            Console.WriteLine(S);
        }
    }
}
